import pytest

from label_layout.label import Label, Point, intersects


@pytest.mark.parametrize(
    ("port_x", "port_y", "point_x", "point_y", "angle"),
    [
        (0, 0, 0, -1, 0),
        (0, 0, 1, -1, 45),
        (0, 0, 1, 0, 90),
        (0, 0, 1, 1, 135),
        (0, 0, 0, 1, 180),
        (0, 0, -1, 1, 225),
        (0, 0, -1, 0, 270),
        (0, 0, -1, -1, 315),
        (
            1.0175301908843006,
            -0.7809213089209193,
            0.5655296229802513,
            -0.2574850299401198,
            220.8114449442811,
        ),
    ],
)
def test_get_angle(port_x: float, port_y: float, point_x: float, point_y: float, angle: float) -> None:
    point = Point(point_x, point_y, 0, None)
    result, radius = point.get_angle(port_x, port_y)
    assert result == angle
    assert 0.0 < radius < 2.0


@pytest.mark.parametrize(
    ("port_x", "port_y", "point_x", "point_y", "angle"),
    [
        (1, 0, 1, -1, 0),
        (1, 0, 0, -1, 45),
        (1, 0, 0, 0, 90),
        (1, 0, 0, 1, 135),
        (1, 0, 1, 1, 180),
        (1, 0, 2, 1, 225),
        (1, 0, 2, 0, 270),
        (1, 0, 2, -1, 315),
    ],
)
def test_get_angle_reversed(port_x: float, port_y: float, point_x: float, point_y: float, angle: float) -> None:
    point = Point(point_x, point_y, 0, None)
    result, radius = point.get_angle(port_x, port_y, start=270, direction=1)
    assert result == angle
    assert 0.0 < radius < 2.0


@pytest.mark.parametrize(
    ("label", "gradient", "intercept"),
    [
        (Label(10, 0, 1, None, 20, 0), 0, 0),
        (Label(10, 10, 1, None, 20, 10), 0, 10),
        (Label(10, 10, 1, None, 20, 20), 1, 0),
        (Label(10, 20, 1, None, 20, 30), 1, 10),
    ],
)
def test_coefficients(label: Label, gradient: float, intercept: float) -> None:
    assert label.coefficients() == (gradient, intercept)


@pytest.mark.parametrize(
    ("label", "is_vertical"),
    [(Label(10, 20, 1, None, 10, 25), True), (Label(10, 20, 1, None, 15, 25), False)],
)
def test_is_vertical(label: Label, is_vertical: bool) -> None:
    assert label.is_vertical == is_vertical


@pytest.mark.parametrize(
    ("label", "minx", "maxx"),
    [
        (Label(10, 20, 1, None, 10, 25), 10, 10),
        (Label(10, 20, 1, None, 15, 25), 10, 15),
        (Label(15, 20, 1, None, 10, 25), 10, 15),
    ],
)
def test_x_bounds(label: Label, minx: float, maxx: float) -> None:
    assert label.x_bounds() == (minx, maxx)


@pytest.mark.parametrize(
    ("other", "result"),
    [
        (Label(10, 10, 1, None, 50, 20), False),
        (Label(10, 30, 1, None, 50, 20), True),
        (Label(40, 0, 1, None, 60, 30), False),
    ],
)
def test_label_intersects(other: Label, result: bool) -> None:
    label = Label(10, 20, 1, None, 50, 30)
    assert label.intersects(other) == result
    assert other.intersects(label) == result


@pytest.mark.parametrize(
    ("start_x", "start_y", "end_x", "end_y", "result"),
    [
        (10, 10, 50, 20, False),
        (10, 30, 50, 20, True),
        (40, 0, 60, 30, False),
        (0, 30, 0, 20, False),
        (60, 40, 60, 20, False),
        (0, 10, 0, 20, False),
        # vertical line is infinitely long...
        (30, 0, 30, 10, True),
        (30, 20, 30, 30, True),
        (30, 30, 30, 50, True),
    ],
)
def test_intersects(start_x: float, start_y: float, end_x: float, end_y: float, result: bool) -> None:
    assert intersects((10, 20), (50, 30), (start_x, start_y), (end_x, end_y)) == result
    assert intersects((start_x, start_y), (end_x, end_y), (10, 20), (50, 30)) == result
