import operator
from statistics import median

from label_layout.label import Label
from label_layout.layout import LabelLayout, LayoutRegion, Leader, Port


def test_one_point_each_side() -> None:
    layout = LabelLayout(100, 80, 0)
    layout.add_point(20, 30, 10, "A")
    layout.add_point(60, 60, 10, "B")
    assert len(layout) == 2
    assert layout.get_labels() == [
        Label(20, 30, 10, "A", 0, 40),
        Label(60, 60, 10, "B", 100, 40),
    ]


def test_two_points_each_side() -> None:
    layout = LabelLayout(100, 80, 0)
    layout.add_point(20, 30, 10, "A")
    layout.add_point(60, 60, 10, "B")
    layout.add_point(70, 40, 10, "C")
    layout.add_point(40, 50, 10, "D")
    assert len(layout) == 4
    assert layout.get_labels() == [
        Label(20, 30, 10, "A", 0, 25),
        Label(40, 50, 10, "D", 0, 55),
        Label(70, 40, 10, "C", 100, 25),
        Label(60, 60, 10, "B", 100, 55),
    ]


def test_all_two_point_variants() -> None:
    points = [(20, 10), (40, 10), (20, 40), (40, 40), (20, 70), (40, 70)]

    expected = [
        ["--", "BA", "BA", "BA", "BA", "BA"],
        ["AB", "--", "BA", "BA", "BA", "BA"],
        ["AB", "AB", "--", "BA", "BA", "BA"],
        ["AB", "AB", "AB", "--", "BA", "BA"],
        ["AB", "AB", "AB", "AB", "--", "AB"],
        ["AB", "AB", "AB", "AB", "BA", "--"],
    ]

    result = [["--"] * 6 for i in range(6)]

    for i in range(6):
        for j in range(6):
            if i == j:
                continue

            layout = LabelLayout(100, 80, 0)
            layout.add_point(points[i][0], points[i][1], 10, "A")
            layout.add_point(points[j][0], points[j][1], 10, "B")
            labels = sorted(layout.get_labels(), key=operator.attrgetter("port_y"), reverse=True)
            result[i][j] = "".join(label.data for label in labels)
            assert not labels[0].intersects(labels[1])

    assert result == expected


def test_vic_map_july_2019() -> None:
    layout = LabelLayout(1114, 835, 20)
    # left
    layout.add_point(520, 650, 104, "A")
    # right
    layout.add_point(630, 620, 104, "B")
    layout.add_point(630, 630, 36, "C")
    layout.add_point(630, 710, 104, "D")

    labels = layout.get_labels()
    assert labels == [
        Label(520, 650, 104, "A", -20, 418),
        Label(630, 620, 104, "B", 1134, 200),
        Label(630, 630, 36, "C", 1134, 418),
        Label(630, 710, 104, "D", 1134, 635),
    ]

    assert intersection(labels) is None


def test_vic_map_july_2019_same_height() -> None:
    layout = LabelLayout(1114, 835, 20)
    # left
    layout.add_point(520, 650, 36, "A")
    # right
    layout.add_point(630, 620, 36, "B")
    layout.add_point(630, 630, 36, "C")
    layout.add_point(630, 710, 36, "D")

    labels = layout.get_labels()
    assert labels == [
        Label(520, 650, 36, "A", -20, 418),
        Label(630, 620, 36, "B", 1134, 200),
        Label(630, 630, 36, "C", 1134, 418),
        Label(630, 710, 36, "D", 1134, 635),
    ]

    assert intersection(labels) is None


def test_shadowed_point_radius_0() -> None:
    layout = LabelLayout(1114, 835, 20, mid_x=0)

    layout.add_point(680, 650, 36, "A")
    layout.add_point(550, 660, 72, "B")
    layout.add_point(560, 660, 104, "C")
    layout.add_point(550, 670, 36, "D")
    layout.add_point(510, 700, 36, "E")
    layout.add_point(530, 700, 36, "F")

    labels = layout.get_labels()
    assert labels == [
        Label(550, 660, 72, "B", 1134, 110),
        Label(560, 660, 104, "C", 1134, 271),
        Label(550, 670, 36, "D", 1134, 415),
        Label(680, 650, 36, "A", 1134, 524),
        Label(530, 700, 36, "F", 1134, 634),
        Label(510, 700, 36, "E", 1134, 743),
    ]

    assert intersection(labels) is None


def intersection(labels: list[Label]) -> tuple[Label, Label] | None:
    for a in labels:
        for b in labels:
            if a is b:
                continue
            if a.intersects(b):
                return (a, b)
    return None


def test_merge_adjacent_separate1() -> None:
    labels = [
        Label(20, 30, 10, "A", 0, 25),
        Label(40, 50, 10, "B", 0, 40),
        Label(70, 40, 10, "C", 0, 60),
        Label(60, 60, 10, "D", 0, 75),
    ]
    assert LabelLayout.merge_adjacent(labels, eq=operator.eq, merge_data="".join) == (
        [
            Leader(20, 30, 0, 25, "A"),
            Leader(40, 50, 0, 40, "B"),
            Leader(70, 40, 0, 60, "C"),
            Leader(60, 60, 0, 75, "D"),
        ],
        [
            Port(0, 25, 10, "A"),
            Port(0, 40, 10, "B"),
            Port(0, 60, 10, "C"),
            Port(0, 75, 10, "D"),
        ],
    )


def test_merge_adjacent_separate2() -> None:
    labels = [
        Label(20, 30, 10, "A", 0, 25),
        Label(40, 50, 10, "B", 0, 40),
        Label(70, 40, 10, "A", 0, 60),
        Label(60, 60, 10, "D", 0, 75),
    ]
    assert LabelLayout.merge_adjacent(labels, eq=operator.eq, merge_data="".join) == (
        [
            Leader(20, 30, 0, 25, "A"),
            Leader(40, 50, 0, 40, "B"),
            Leader(70, 40, 0, 60, "A"),
            Leader(60, 60, 0, 75, "D"),
        ],
        [
            Port(0, 25, 10, "A"),
            Port(0, 40, 10, "B"),
            Port(0, 60, 10, "A"),
            Port(0, 75, 10, "D"),
        ],
    )


def test_merge_adjacent1() -> None:
    labels = [
        Label(20, 30, 10, "A", 0, 25),
        Label(40, 50, 10, "A", 0, 40),
        Label(40, 50, 10, "A", 0, 40),
        Label(70, 40, 10, "C", 0, 60),
        Label(60, 60, 10, "D", 0, 75),
    ]
    assert LabelLayout.merge_adjacent(labels, eq=operator.eq, merge_data="".join) == (
        [
            Leader(20, 30, 0, 32.5, "AA"),
            Leader(40, 50, 0, 32.5, "AA"),
            Leader(70, 40, 0, 60, "C"),
            Leader(60, 60, 0, 75, "D"),
        ],
        [Port(0, 32.5, 10, "AA"), Port(0, 60, 10, "C"), Port(0, 75, 10, "D")],
    )


def test_get_merged_labels() -> None:
    layout = LabelLayout(100, 80, 0)
    layout.add_point(20, 30, 10, "A")
    layout.add_point(60, 60, 10, "B")
    layout.add_point(70, 40, 10, "C")
    layout.add_point(40, 50, 10, "D")
    assert layout.get_labels() == [
        Label(20, 30, 10, "A", 0, 25),
        Label(40, 50, 10, "D", 0, 55),
        Label(70, 40, 10, "C", 100, 25),
        Label(60, 60, 10, "B", 100, 55),
    ]

    assert layout.get_merged_labels(eq=operator.eq, merge_data="".join) == (
        [
            Leader(20, 30, 0, 25, "A"),
            Leader(40, 50, 0, 55, "D"),
            Leader(70, 40, 100, 25, "C"),
            Leader(60, 60, 100, 55, "B"),
        ],
        [
            Port(0, 25, 10, "A"),
            Port(0, 55, 10, "D"),
            Port(100, 25, 10, "C"),
            Port(100, 55, 10, "B"),
        ],
    )


def test_get_merged_labels2() -> None:
    mid_x = median(
        [
            689,
            727,
            712,
            865,
            1010,
            1056,
            1248,
            1187,
            1026,
        ]
    )
    layout = LabelLayout(1269, 951, 0, mid_x=mid_x)
    layout.add_point(689, 309, 10, "L1")
    layout.add_point(727, 420, 10, "L2")
    layout.add_point(712, 438, 10, "L3")
    layout.add_point(865, 509, 10, "L4")
    layout.add_point(1010, 496, 10, "R1")
    layout.add_point(1056, 505, 10, "R2")
    layout.add_point(1248, 625, 10, "R3")
    layout.add_point(1187, 634, 10, "R4")
    layout.add_point(1026, 603, 10, "R5")

    assert layout.get_labels() == [
        Label(689, 309, 10, "L1", 0, 187.0),
        Label(727, 420, 10, "L2", 0, 379.0),
        Label(712, 438, 10, "L3", 0, 572.0),
        Label(865, 509, 10, "L4", 0, 764.0),
        Label(1010, 496, 10, "R1", 1269, 155.0),
        Label(1056, 505, 10, "R2", 1269, 315.0),
        Label(1248, 625, 10, "R3", 1269, 476.0),
        Label(1187, 634, 10, "R4", 1269, 636.0),
        Label(1026, 603, 10, "R5", 1269, 796.0),
    ]

    assert layout.get_merged_labels(eq=operator.eq, merge_data="".join) == (
        [
            Leader(689, 309, 0, 187.0, "L1"),
            Leader(727, 420, 0, 379.0, "L2"),
            Leader(712, 438, 0, 572.0, "L3"),
            Leader(865, 509, 0, 764.0, "L4"),
            Leader(1010, 496, 1269, 155.0, "R1"),
            Leader(1056, 505, 1269, 315.0, "R2"),
            Leader(1248, 625, 1269, 476.0, "R3"),
            Leader(1187, 634, 1269, 636.0, "R4"),
            Leader(1026, 603, 1269, 796.0, "R5"),
        ],
        [
            Port(0, 187.0, 10, "L1"),
            Port(0, 379.0, 10, "L2"),
            Port(0, 572.0, 10, "L3"),
            Port(0, 764.0, 10, "L4"),
            Port(1269, 155.0, 10, "R1"),
            Port(1269, 315.0, 10, "R2"),
            Port(1269, 476.0, 10, "R3"),
            Port(1269, 636.0, 10, "R4"),
            Port(1269, 796.0, 10, "R5"),
        ],
    )


def test_get_merged_labels2reverse() -> None:
    mid_x = median(
        [
            689,
            727,
            712,
            865,
            1010,
            1056,
            1248,
            1187,
            1026,
        ]
    )
    layout = LabelLayout(1269, 951, 0, mid_x=mid_x, lhs_order="bottom", rhs_order="bottom")
    layout.add_point(689, 309, 10, "L1")
    layout.add_point(727, 420, 10, "L2")
    layout.add_point(712, 438, 10, "L3")
    layout.add_point(865, 509, 10, "L4")
    layout.add_point(1010, 496, 10, "R1")
    layout.add_point(1056, 505, 10, "R2")
    layout.add_point(1248, 625, 10, "R3")
    layout.add_point(1187, 634, 10, "R4")
    layout.add_point(1026, 603, 10, "R5")

    assert [layout_.data for layout_ in layout.get_labels()] == [
        "L1",
        "L2",
        "L3",
        "L4",
        "R1",
        "R2",
        "R5",
        "R3",
        "R4",
    ]

    assert layout.get_labels() == [
        Label(689, 309, 10, "L1", 0, 187.0),
        Label(727, 420, 10, "L2", 0, 379.0),
        Label(712, 438, 10, "L3", 0, 572.0),
        Label(865, 509, 10, "L4", 0, 764.0),
        Label(1010, 496, 10, "R1", 1269, 155.0),
        Label(1056, 505, 10, "R2", 1269, 315.0),
        Label(1026, 603, 10, "R5", 1269, 476.0),
        Label(1248, 625, 10, "R3", 1269, 636.0),
        Label(1187, 634, 10, "R4", 1269, 796.0),
    ]

    assert layout.get_merged_labels(eq=operator.eq, merge_data="".join) == (
        [
            Leader(689, 309, 0, 187.0, "L1"),
            Leader(727, 420, 0, 379.0, "L2"),
            Leader(712, 438, 0, 572.0, "L3"),
            Leader(865, 509, 0, 764.0, "L4"),
            Leader(1010, 496, 1269, 155.0, "R1"),
            Leader(1056, 505, 1269, 315.0, "R2"),
            Leader(1026, 603, 1269, 476.0, "R5"),
            Leader(1248, 625, 1269, 636.0, "R3"),
            Leader(1187, 634, 1269, 796.0, "R4"),
        ],
        [
            Port(0, 187.0, 10, "L1"),
            Port(0, 379.0, 10, "L2"),
            Port(0, 572.0, 10, "L3"),
            Port(0, 764.0, 10, "L4"),
            Port(1269, 155.0, 10, "R1"),
            Port(1269, 315.0, 10, "R2"),
            Port(1269, 476.0, 10, "R5"),
            Port(1269, 636.0, 10, "R3"),
            Port(1269, 796.0, 10, "R4"),
        ],
    )


def test_phase1_ports() -> None:
    assert LayoutRegion._get_phase1_ports(1200, 5, reverse=False) == [
        (200.0, 400.0),
        (400.0, 600.0),
        (600.0, 800.0),
        (800.0, 1000.0),
        (1000.0, 1200.0),
    ]
    assert LayoutRegion._get_phase1_ports(1200, 5, reverse=True) == [
        (1000.0, 800.0),
        (800.0, 600.0),
        (600.0, 400.0),
        (400.0, 200.0),
        (200.0, 0.0),
    ]


def test_phase2_ports() -> None:
    labels = [
        Label(1, 2, 0, "L1", 0, 200.0),
        Label(3, 4, 0, "L2", 0, 400.0),
        Label(5, 6, 0, "L3", 0, 600.0),
        Label(7, 8, 0, "L4", 0, 800.0),
        Label(9, 1, 0, "L5", 0, 1000.0),
    ]
    assert LayoutRegion._get_phase2_ports(1200, labels, 0) == [
        (200.0, 400.0),
        (400.0, 600.0),
        (600.0, 800.0),
        (800.0, 1000.0),
        (1000.0, 1200.0),
    ]

    labels = [
        Label(1, 2, 20, "L1", 0, 200.0),
        Label(3, 4, 20, "L2", 0, 400.0),
        Label(5, 6, 20, "L3", 0, 600.0),
        Label(7, 8, 20, "L4", 0, 800.0),
        Label(9, 1, 20, "L5", 0, 1000.0),
    ]
    assert LayoutRegion._get_phase2_ports(1200, labels, 0) == [
        (193.0, 397.0),
        (397.0, 600.0),
        (600.0, 803.0),
        (803.0, 1007.0),
        (1007.0, 1200.0),
    ]

    labels = [
        Label(1, 2, 20, "L1", 0, 200.0),
        Label(3, 4, 40, "L2", 0, 400.0),
        Label(5, 6, 60, "L3", 0, 600.0),
        Label(7, 8, 80, "L4", 0, 800.0),
        Label(9, 1, 100, "L5", 0, 1000.0),
    ]
    assert LayoutRegion._get_phase2_ports(1200, labels, 0) == [
        (160.0, 340.0),
        (340.0, 540.0),
        (540.0, 760.0),
        (760.0, 1000.0),
        (1000.0, 1200.0),
    ]
