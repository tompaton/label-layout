# Label Layout

Compute positions of labels for points on an image, such that the lines
(leaders) between point and label (port) don't overlap.

Quick & dirty heuristic version - divides labels into left & right halves, then
equally spaces the labels picking points that don't overlap, and then moving
the labels around a little once we know how high each label will be.


[![PyPI - Version](https://img.shields.io/pypi/v/label-layout.svg)](https://pypi.org/project/label-layout)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/label-layout.svg)](https://pypi.org/project/label-layout)

-----

## Table of Contents

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install label-layout
```

## License

`label-layout` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
