from collections.abc import Callable, Iterable
from dataclasses import dataclass
from fractions import Fraction
from itertools import pairwise
from math import asin, degrees
from statistics import median
from typing import Any, TypeVar

from label_layout.label import Label, Point


@dataclass
class Leader:
    x: float
    y: float
    port_x: float
    port_y: float
    data: Any


@dataclass
class Port:
    port_x: float
    port_y: float
    height: float
    data: Any


class LabelLayout:
    """Layout labels for points of interest on an image.

    Image has dimensions (width, height).

    Labels will be positioned margin units off the edge of each side of the
    image.

    Y coordinates will be rounded to precision decimal places (default 0).
    """

    def __init__(
        self,
        width: float,
        height: float,
        margin: float = 0.0,
        mid_x: float | None = None,
        precision: int = 0,
        lhs_order: str = "top",
        rhs_order: str = "top",
        marker_radius: Fraction = Fraction(0),
    ) -> None:
        self.width = width

        if mid_x is None:
            self.mid_x = self.width / 2.0
        else:
            self.mid_x = mid_x
        self.left_half = LayoutRegion(
            height,
            port_x=0 - margin,
            side="left",
            precision=precision,
            reverse=lhs_order != "top",
            marker_radius=marker_radius,
        )
        self.right_half = LayoutRegion(
            height,
            port_x=width + margin,
            side="right",
            precision=precision,
            reverse=rhs_order != "top",
            marker_radius=marker_radius,
        )

    def __len__(self) -> int:
        return len(self.left_half) + len(self.right_half)

    def add_point(self, point_x: float, point_y: float, label_height: float, data: Any) -> None:
        """Add a point of interest to the image."""
        if point_x < self.mid_x:
            self.left_half.add_point(point_x, point_y, label_height, data)
        else:
            self.right_half.add_point(point_x, point_y, label_height, data)

    def get_labels(self) -> list[Label]:
        """Return list of positioned labels for points of interest."""
        return self.left_half.get_labels() + self.right_half.get_labels()

    def get_merged_labels(
        self, eq: Callable[[Any, Any], bool], merge_data: Callable[[list[Any]], Any]
    ) -> tuple[list[Leader], list[Port]]:
        """Return list of positioned labels for points of interest."""
        left_leaders, left_ports = self.merge_adjacent(self.left_half.get_labels(), eq, merge_data)
        right_leaders, right_ports = self.merge_adjacent(self.right_half.get_labels(), eq, merge_data)

        return (left_leaders + right_leaders, left_ports + right_ports)

    @staticmethod
    def merge_adjacent(
        labels: list[Label],
        eq: Callable[[Any, Any], bool],
        merge_data: Callable[[list[Any]], Any],
    ) -> tuple[list[Leader], list[Port]]:
        def continue_range(stack: list[Label], label: Label) -> bool:
            if stack and eq(stack[-1].data, label.data):
                return True

            return False

        def process_items(
            stack: list[Label],
        ) -> Iterable[tuple[Leader | None, Port | None]]:
            if not stack:
                return

            port_x = median(label.port_x for label in stack)
            port_y = median(label.port_y for label in stack)
            data = merge_data([label.data for label in stack])

            for i, label in enumerate(stack):
                yield (
                    Leader(label.x, label.y, port_x, port_y, data),
                    Port(port_x, port_y, label.height, data) if i == 0 else None,
                )

        leaders: list[Leader] = []
        ports: list[Port] = []

        for leader, port in collapse_items(labels, continue_range, process_items):
            if leader:
                leaders.append(leader)

            if port:
                ports.append(port)

        return leaders, ports


Item = TypeVar("Item")
Result = TypeVar("Result")


def collapse_items(
    items: Iterable[Item],
    continue_range: Callable[[list[Item], Item], bool],
    process_items: Callable[[list[Item]], Iterable[Result]],
) -> Iterable[Result]:
    stack: list[Item] = []

    for item in items:
        # skip duplicates
        if stack and stack[-1] == item:
            continue

        if not continue_range(stack, item):
            yield from process_items(stack)
            stack = []

        stack.append(item)

    yield from process_items(stack)


class LayoutRegion:
    """Layout labels for subset of points in region of the image.
    (e.g. left and right halves.)
    """

    def __init__(
        self,
        height: float,
        *,
        port_x: float,
        side: str,
        precision: int = 0,
        reverse: bool = False,
        marker_radius: Fraction = Fraction(0),
    ) -> None:
        self.height = height
        self.port_x = port_x
        self.reverse = reverse
        if side == "left":
            if self.reverse:
                self.direction = 1
                self.start_angle = 270
            else:
                self.direction = -1
                self.start_angle = 90
        elif self.reverse:
            self.direction = -1
            self.start_angle = 270
        else:
            self.direction = 1
            self.start_angle = 90
        self.precision = precision
        self.points: list[Point] = []
        self.marker_radius = marker_radius

    def __len__(self) -> int:
        return len(self.points)

    def add_point(self, point_x: float, point_y: float, label_height: float, data: Any) -> None:
        """Add a point of interest to the image region."""
        self.points.append(Point(point_x, point_y, label_height, data))

    def get_labels(self) -> list[Label]:
        """Yields positioned labels for points of interest."""
        # phase 1: equally spaced ports
        ports = self._get_phase1_ports(self.height, len(self.points), reverse=self.reverse)
        labels = self._select_labels(self.points[:], ports)

        # TODO: phase 2: get better points based on new port positions
        #       and iterate until it stabilizes
        # for i in range(5):
        #     ports = self._get_phase2_ports(self.height, labels, self.precision)
        #     labels = self._select_labels(self.points[:], ports)

        # phase 3: update port positions based on label heights
        ports = self._get_phase2_ports(self.height, labels, self.precision)
        for label, (port_y, _next_port_y) in zip(labels, ports, strict=False):
            label.port_y = port_y

        return labels

    @staticmethod
    def _get_phase1_ports(height: float, num_points: int, *, reverse: bool = False) -> list[tuple[float, float]]:
        # phase 1: equally spaced ports
        space = height / (num_points + 1)
        port_y = 0.0

        if reverse:
            space *= -1
            port_y = height

        return [(port_y + space * (i + 1), port_y + space * (i + 2)) for i in range(num_points)]

    @staticmethod
    def _get_phase2_ports(height: float, labels: list[Label], precision: int = 0) -> list[tuple[float, float]]:
        # phase 2: update port positions based on label heights
        total_height = sum(label.height for label in labels)
        space = (height - total_height) / (len(labels) + 1)

        ports = []
        base_y = 0.0
        for label in labels:
            base_y += space
            ports.append(round(base_y + 0.5 * label.height, precision))
            base_y += label.height

        ports.append(base_y + space)

        return list(pairwise(ports))

    def _select_labels(self, points: list[Point], ports: Iterable[tuple[float, float]]) -> list[Label]:
        labels = []

        for port_y, next_port_y in ports:
            # get point based on next port position, that way we
            # guarantee the line from this port to the point can't
            # cross any line from the next port.
            point = self.get_point(points, self.port_x, next_port_y)

            labels.append(Label(point.x, point.y, point.height, point.data, self.port_x, port_y))

        # make sure labels still go from top to bottom
        return sorted(labels, key=lambda label: label.port_y)

    def get_point(self, points: list[Point], port_x: float, port_y: float) -> Point:
        """Find Point with smallest angle from port."""
        # "proper" algorithm is too complex: https://arxiv.org/pdf/1702.01799.pdf
        angles = []

        # adjust port_y by marker_radius
        if self.reverse:
            port_y += self.marker_radius
        else:
            port_y -= self.marker_radius

        for point in points:
            alpha, radius = point.get_angle(port_x, port_y, start=self.start_angle, direction=self.direction)

            # adjust angle by marker_radius to get tangent
            if radius > self.marker_radius * 5:
                alpha += degrees(asin(self.marker_radius / radius))

            angles.append((point, alpha, radius))

        angles.sort(key=lambda x: x[1:])

        best = angles[0]

        points.remove(best[0])

        return best[0]
