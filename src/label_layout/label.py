from dataclasses import dataclass
from math import atan2, degrees, hypot
from typing import Any


@dataclass
class Point:
    """Point of interest on image.

    As per Label but position of port is not yet known.
    """

    x: float
    y: float
    height: float
    data: Any

    def get_angle(
        self, port_x: float, port_y: float, start: float = 90, direction: float = -1.0
    ) -> tuple[float, float]:
        """Return angle in degrees between point and (port_x, port_y)
        and length of line (radius).
        (0, 0) is top-left.
        clockwise for direction = -1, and start = 90 to make 0 degrees at top.
        anti-clockwise for direction = 1, and start = 270 to make 0 degrees at bottom.

        """
        angle = start + direction * degrees(atan2(-(self.y - port_y), self.x - port_x))

        radius = hypot(self.x - port_x, -(self.y - port_y))

        return angle % 360, radius


@dataclass
class Label(Point):
    """Label for a point of interest on image.

    (x, y) - coordinates of point, (0, 0) at top-left.

    height - vertical space required for label.

    data - data required to render the label.

    (port_x, port_y) - center-point of label port.

    I.e. caller should render a line between (x, y) to (port_x, port_y).
    """

    port_x: float
    port_y: float

    def coefficients(self) -> tuple[float, float]:
        """Return coefficients of leader line, y = mx + c"""
        gradient = (self.y - self.port_y) / (self.x - self.port_x)
        intercept = self.y - self.x * gradient
        return gradient, intercept

    @property
    def is_vertical(self) -> bool:
        return self.x == self.port_x

    def x_bounds(self) -> tuple[float, float]:
        """Return x interval of leader line"""
        return min(self.x, self.port_x), max(self.x, self.port_x)

    def intersects(self, other: "Label") -> bool:
        """Test if other leader line intersects this leader line."""
        m1, c1 = self.coefficients()
        m2, c2 = other.coefficients()

        if m1 == m2:  # parallel
            return False

        # intersection x, y is on both lines
        # y = m1 * x + c1
        # y = m2 * x + c2
        # m1 * x + c1 = m2 * x + c2
        # x * (m1 - m2) = c2 - c1
        x = (c2 - c1) / (m1 - m2)

        lower1, upper1 = self.x_bounds()
        lower2, upper2 = other.x_bounds()

        return max(lower1, lower2) <= x <= min(upper1, upper2)


def intersects(
    start_a: tuple[float, float],
    end_a: tuple[float, float],
    start_b: tuple[float, float],
    end_b: tuple[float, float],
) -> bool:
    line_a = Label(start_a[0], start_a[1], 0.0, None, end_a[0], end_a[1])
    line_b = Label(start_b[0], start_b[1], 0.0, None, end_b[0], end_b[1])

    if line_a.is_vertical:
        lower, upper = line_b.x_bounds()
        return lower <= line_a.x <= upper

    if line_b.is_vertical:
        lower, upper = line_a.x_bounds()
        return lower <= line_b.x <= upper

    return line_a.intersects(line_b)
