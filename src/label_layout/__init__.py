# SPDX-FileCopyrightText: 2024-present Tom Paton <tom.paton@gmail.com>
#
# SPDX-License-Identifier: MIT
"""Label layout

Compute positions of labels for points on an image, such that the
lines (leaders) between point and label (port) don't overlap.
"""

from label_layout.label import intersects
from label_layout.layout import LabelLayout, Leader, Port, collapse_items

__all__ = ["intersects", "LabelLayout", "collapse_items", "Leader", "Port"]
